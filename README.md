Fifty Play is the number guessing game for Android.

#How to play#
Fifty Play app guesses the number you have chosen. The rules are, you pick any whole number
between 1 and 50 and keep that as a secret. Then on step 1 you will check the table of numbers that
are presented to you, if your secret number is among those numbers you click the Yes button.
Otherwise you click the No button. You continue the next seven steps. And your secret number is
finally revealed to you.

#Screenshots#
![screenshot01](https://bitbucket.org/mkhanyisi/fifty-play/raw/master/screenshots/screenshot01.png)

![screenshot02](https://bitbucket.org/mkhanyisi/fifty-play/raw/master/screenshots/screenshot02.png)

![screenshot03](https://bitbucket.org/mkhanyisi/fifty-play/raw/master/screenshots/screenshot03.png)
